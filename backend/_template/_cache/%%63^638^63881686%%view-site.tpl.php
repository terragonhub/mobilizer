<?php /* Smarty version 2.6.26, created on 2013-05-03 15:11:11
         compiled from view-site.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!--Content-->
<div id="content">
	<div class="container clearfix">
	
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "side-bar.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    
    <div class="box three_fourth last">
 		<div class="header">
 			<h2>List of Sites you already created</h2>
    <!--Toggle-->
    <span class="toggle"></span>
 		</div>
 		<div class="content padding">
        
        <!--Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. -->
         <?php if ($this->_tpl_vars['error']): ?><p class="message invalid"><?php echo $this->_tpl_vars['error']; ?>
<span class="close">X</span></p><?php endif; ?>
         <?php if ($this->_tpl_vars['okay']): ?><p class="message valid"><?php echo $this->_tpl_vars['okay']; ?>
<span class="close">X</span></p><?php endif; ?>
         
         <div class="dataTables_wrapper">
         <table>
             <thead>
              <tr>
               <th>S/N</th>
               <th style="width:80px"></th>
               <th style="width: 120px;"> Site Name </th>
               <th class="sorting" style="width: 140px;">Alias</th>
               <th class="sorting" style="width: 160px;">Site Address</th>
               <th class="sorting" style="width: 30px;">Template</th>
               <th class="sorting" style="width: 30px;">Scheme</th>
               <th style="width:80px"></th>
              </tr>
             </thead>
             
            <tbody>
            <?php $_from = $this->_tpl_vars['allSites']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['eachKey'] => $this->_tpl_vars['eachSite']):
?>
              <tr class="gradeA odd">
                   <td><?php echo $this->_tpl_vars['eachKey']+1; ?>
</td>
                   <td>
                   <a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
site-detials/<?php echo $this->_tpl_vars['eachSite']->id; ?>
/<?php echo $this->_tpl_vars['eachSite']->alias; ?>
/" title="View & Enhance <?php echo $this->_tpl_vars['eachSite']->name; ?>
">
                        <img src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/img/view.png" />
                    </a><a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
create-page/<?php echo $this->_tpl_vars['eachSite']->id; ?>
/<?php echo $this->_tpl_vars['eachSite']->alias; ?>
/" title="Add Page to <?php echo $this->_tpl_vars['eachSite']->name; ?>
">
                        <img src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/img/edit_add.png" />
                    </a>
                   </td>
                   <td class="sorting_1">
                    <a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
view-sites/<?php echo $this->_tpl_vars['eachSite']->id; ?>
/<?php echo $this->_tpl_vars['eachSite']->alias; ?>
/"><?php echo $this->_tpl_vars['eachSite']->name; ?>
</a>
                    </td>
                   <td><?php echo $this->_tpl_vars['eachSite']->alias; ?>
</td>
                   <td><?php echo $this->_tpl_vars['eachSite']->mainAddress; ?>
</td>
                   <td><?php echo $this->_tpl_vars['eachSite']->template; ?>
</td>
                   <td><?php echo $this->_tpl_vars['eachSite']->colourSchemes; ?>
</td>
                   <td>
                    <a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
view-sites/<?php echo $this->_tpl_vars['eachSite']->id; ?>
/<?php echo $this->_tpl_vars['eachSite']->alias; ?>
/"><img src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/img/pen.png" title="Edit" /></a>
                    <a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
view-sites/<?php echo $this->_tpl_vars['eachSite']->id; ?>
/delete/" onclick="if(!confirm('Are you sure you want to delete?'))return false;"><img src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/img/delete.png" title="Delete" /></a>
                   </td>
              </tr>
            <?php endforeach; endif; unset($_from); ?>
            </tbody>
      </table>
      
      <!--<div class="dataTables_info">Showing 1 to 10 of 50 entries</div>
      <div class="dataTables_paginate paging_full_numbers"><span class="first paginate_button">First</span><span class="previous paginate_button">Previous</span><span><span class="paginate_active">1</span><span class="paginate_button">2</span><span class="paginate_button">3</span><span class="paginate_button">4</span><span class="paginate_button">5</span></span><span class="next paginate_button">Next</span><span class="last paginate_button">Last</span></div>-->
      
      </div>
         
         
         
         </div>
 	</div>
    
    
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>