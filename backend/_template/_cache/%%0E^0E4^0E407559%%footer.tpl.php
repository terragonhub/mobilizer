<?php /* Smarty version 2.6.26, created on 2013-05-03 12:44:30
         compiled from footer.tpl */ ?>
    <!-- these are modal boxes used in this application... -->
    <div id="create-new-site" style="display:none;">
        <form id="create-site" method="post" class="create-site" action="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
view-sites/">
         <fieldset>
          <legend>Create a New Site</legend>
          
            <div class="field">
                <label>Site Name</label>
                <input type="text" name="site_name" id="site_name" class="ex-large" onchange="setAlias()">
            </div>
            
            <input type="hidden" name="user_id" id="user_id" value="<?php echo $this->_tpl_vars['loggedInfo']->id; ?>
" class="large" />
            
            <div class="field">
            <label>Site Template</label>
            <select id="site_template" name="site_template">
                <option value=""> &rarr; Choose Template &larr; </option>
                <option value="0" selected>Dummy Template</option>
                <option value="1">Other Template</option>
            </select>
            </div>
            
            <div class="field">
                <label>Alias</label>
                <input type="text" name="site_alias" id="site_alias" class="ex-large" >
            </div>
            
            <div class="field">
            <label>Color Scheme</label>
            <select id="color_scheme" name="color_scheme">
                <option value=""> &rarr; Choose Scheme &larr; </option>
                <option value="0">Dummy Scheme</option>
                <option value="1">Other Scheme</option>
            </select>
            </div>
              
            <div class="field">
                <label>Site Main Address</label>
                <input type="text" name="main_address" id="main_address" class="ex-large">
            </div>
            
          <button type="reset" class="secondary">Reset</button>
          <button onclick="registerSite();">Add Site</button>
          <!--<button disabled="">Disabled Button</button>-->
         </fieldset>
        </form>
        
        
        
    </div>
    <!-- end of modal boxes okay... -->
<!--Footer-->
   <div id="footer" class="separator"> 
   <!-- Remove this notice or replace it with whatever you want --> 
   &#169; Copyright 2013 <?php echo $this->_tpl_vars['SITE_NAME']; ?>
 | Powered by <a href="#">Terragon Tech</a> | <a href="#">Top</a> 
   </div>
  <!--End .container--> 
 </div>
 <!--End #content--> 
</div>
</body>
</html>