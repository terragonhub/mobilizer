<?php /* Smarty version 2.6.26, created on 2013-05-02 15:03:15
         compiled from side-bar.tpl */ ?>
<div class="box one_fourth">
		   <div class="header">
			<h2><a href="#">Choose a Task</a></h2>
			<!--<span class="draggable"></span> <span class="toggle"></span>-->
			</div>
		   <div class="content">
			<div class="accordion">
			
			 <h2 class="panel active"><a href="#">Select Template</a></h2>
			 <div class="content" style="display: block;">
			  <h6><a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
somewhere/">Select Template</a></h6>
			  <p>Choose from the list of templates.</p>
			 </div>
			 
			 <h2 class="panel"><a href="#">Create Pages</a></h2>
			 <div class="content" style="display: none;">
			  <h6><a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
create-pages/">Create Pages</a></h6>
			  <p> Begin page creation from here.</p>
			 </div>
			 
			 <h2 class="panel"><a href="#">Add Pages</a></h2>
			 <div class="content" style="display: none;">
			  <h6><a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
add-pages/">Add Pages</a></h6>
			  <p>Unique Page creation goes here.</p>
			 </div>
			 
			 <h2 class="panel"><a href="#">Add Content</a></h2>
			 <div class="content" style="display: none;">
			  <h6><a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
add-content/">Add Content</a></h6>
			  <p>Page content goes here.</p>
			 </div>
			 
			</div>
		   </div>
  </div>