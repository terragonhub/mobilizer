<?php /* Smarty version 2.6.26, created on 2013-04-30 14:00:12
         compiled from index.tpl */ ?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><?php echo $this->_tpl_vars['SITE_NAME']; ?>
</title>
	<!--CSS-->
	<link rel="stylesheet" href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/css/reset.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/css/styles.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/css/add_css.css" type="text/css" />
	<!-- Google Fonts -->
	<!--<link href='http://fonts.googleapis.com/css?family=Droid+Serif:regular,italic' rel='stylesheet' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:regular,bold' rel='stylesheet' type='text/css' />-->
	<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/jquery.validate.js" type="text/javascript"></script>
	<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/superfish.js" type="text/javascript"></script>
	<!--jQuery Excanvas-->
	<!--[if IE]><script src="plugins/jquery-excanvas/excanvas.js" type="text/javascript"></script><![endif]-->
	<!--jQuery Visualize-->
	<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/visualize.jQuery.js" type="text/javascript"></script>
	<link href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/visualize.css" rel="stylesheet" type="text/css">
	<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/jquery.uniform.min.js" type="text/javascript"></script>
	<link href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/css/uniform.default.css" rel="stylesheet" type="text/css">
	<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/jquery.tipsy.js" type="text/javascript"></script>
	<link href='<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/tipsy.css' rel='stylesheet' type='text/css' />
	<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/facebox.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/facebox.css" type="text/css" />
	<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/loader.js" type="text/javascript"></script>
</head>

<body>

<!--Content-->
<div id="content">
<div class="login-wrap">
	<div id="login" class="container"> 
	<!--<img class="logo" src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/img/logo.png" alt="logo" />-->
	<!--Login-->
	<div class="box">
	<div class="header">
	<h2>USER LOGIN <small style="font-weight:normal;">[valid username or email and password only]</small></h2>
	</div>
	<div class="content">
	<div class="tabs"> 
	 <!--navigation-->
	 <ul class="navigation clearfix">
	  <li> <a class="current" href="#tab1">Login</a></li>
	  <li><a href="#tab2">Register</a></li>
	  <li><a href="#tab3">Recover Password</a></li>
	 </ul>
	 <!--tab1-->
	 
	 <div class="tab" id="tab1">
	  <!--<p class="message valid">
		Sucess Message
	  <span class="close">X</span>
	  </p>
	  <p class="message invalid">
		Error Message
	  <span class="close">X</span>
	  </p>-->
	  <?php if ($this->_tpl_vars['error']): ?><p class="message invalid"><?php echo $this->_tpl_vars['error']; ?>
<span class="close">X</span></p><?php endif; ?>
	  <form action="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
" method="post" class="form" id="login-form">
		   <p class="field">
			  <label>Username or Email Address</label>
			  <input type="text" class="large" name="username" id="username" <?php echo $_POST['username']; ?>
>
		   </p>
		 <p class="field">
		  <label for="username">Password </label>
		  <input type="password" id="password" name="password" <?php echo $_POST['password']; ?>
 class="large">
		 </p>
		 <p class="field">
			<button type="reset" class="secondary">Reset</button>
			<button type="submit">Submit</button>
		 </p>
	   </form>
	  <?php echo '
		<script type="text/javascript">
			$("#login-form").validate({
				rules: {
					username: { required: true},
					password: { required: true}
				}
			});
		</script>
		'; ?>

	   
	 </div>
	 
	 
	 <!--tab2-->
	 <div class="tab" id="tab2">
		<form action="#" method="post" class="form">
	 <p class="field">
	  <label for="username">Username </label>
	  <input id="username" name="username" class="large">
	 </p>
	 <p class="field">
	  <label for="username">Password </label>
	  <input name="email" type="password" class="large" id="email">
	</p>
	 <p class="field">
	  <button type="submit">Submit</button>
	  <button type="reset" class="secondary">Reset</button>
	 </p>
	</form>
	 </div>
	 <!--tab3-->
	 
	 
	 <div class="tab" id="tab3">
		<form action="#" method="post" class="form">
		 <p>Your password will be sent to your email account.</p>
		 <p class="field">
		  <label for="username">Email </label>
		  <input id="username" name="username" class="large">
		  </p>
		 <p class="field">
		  <button type="submit">Submit</button>
		  <button type="reset" class="secondary">Reset</button>
		 </p>
		</form>
	 </div>
	 
	<!--End .tabs-->
	</div>    	
	<!--End .content-->	
	</div>
	<!--End .box-->
	</div>
	<!--End .container-->
	</div>
	<div class="clear"></div>
 </div>
<!--End #content-->
</div>
</body>
</html>