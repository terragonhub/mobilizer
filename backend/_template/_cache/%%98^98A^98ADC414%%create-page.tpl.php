<?php /* Smarty version 2.6.26, created on 2013-05-03 17:57:23
         compiled from create-page.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'create-page.tpl', 10, false),array('modifier', 'capitalize', 'create-page.tpl', 10, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!--Content-->
<div id="content">
	<div class="container clearfix">
	
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "side-bar.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    
    <div class="box three_fourth last">
 		<div class="header">
 			<h2>Create Page on <?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['CURRENT_EXTRA'])) ? $this->_run_mod_handler('replace', true, $_tmp, '_', ' ') : smarty_modifier_replace($_tmp, '_', ' ')))) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</h2>
    <!--Toggle-->
    <span class="toggle"></span>
 		</div>
 		<div class="content padding">
        
         <?php if ($this->_tpl_vars['error']): ?><p class="message invalid"><?php echo $this->_tpl_vars['error']; ?>
<span class="close">X</span></p><?php endif; ?>
         <?php if ($this->_tpl_vars['okay']): ?><p class="message valid"><?php echo $this->_tpl_vars['okay']; ?>
<span class="close">X</span></p><?php endif; ?>
         
         
         <form action="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
create-page/<?php echo $this->_tpl_vars['CURRENT_ID']; ?>
/<?php echo $this->_tpl_vars['CURRENT_EXTRA']; ?>
/" method="post" id="create-page">
     <fieldset>
        <legend>Provide Page Details</legend>
        
        <div class="field">
            <label>Page Alias</label>
            <input type="text" name="page_alias" id="page_alias" class="large" <?php echo $_POST['page_alias']; ?>
 />
        </div>
        
        <input type="hidden" value="<?php echo $this->_tpl_vars['CURRENT_ID']; ?>
" id="site_id" name="site_id" <?php echo $_POST['site_id']; ?>
 />
        
        <div class="field">
            <label>Comment: <strong><small>[optional]</small></strong></label>
            <textarea rows="7" cols="50" name="comment" id="comment" <?php echo $_POST['comment']; ?>
></textarea>
        </div>
        
        <button type="reset" class="secondary">Reset</button>
        <button>Create Page</button>
     </fieldset>
    </form>
    <?php echo '
        <script type="text/javascript">
        $("#create-page").validate({
    		rules: {
    			page_alias: { required: true }
    		}
    	});
        </script>
    '; ?>
 
     </div>
 	</div>
    
    
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>