<?php /* Smarty version 2.6.26, created on 2013-05-06 13:04:17
         compiled from site-details.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'site-details.tpl', 10, false),array('modifier', 'capitalize', 'site-details.tpl', 10, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!--Content-->
<div id="content">
	<div class="container clearfix">
	
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "side-bar.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    
    <div class="box three_fourth last">
 		<div class="header">
 			<h2>More Info on <?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['CURRENT_EXTRA'])) ? $this->_run_mod_handler('replace', true, $_tmp, '_', ' ') : smarty_modifier_replace($_tmp, '_', ' ')))) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</h2>
            <!--Toggle-->
            <span class="toggle"></span>
 		</div>
 		<div class="content padding">
        
         <?php if ($this->_tpl_vars['error']): ?><p class="message invalid"><?php echo $this->_tpl_vars['error']; ?>
<span class="close">X</span></p><?php endif; ?>
         <?php if ($this->_tpl_vars['okay']): ?><p class="message valid"><?php echo $this->_tpl_vars['okay']; ?>
<span class="close">X</span></p><?php endif; ?>
         
         
         <form action="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
<?php echo $this->_tpl_vars['CURRENT_PAGE']; ?>
/<?php echo $this->_tpl_vars['CURRENT_ID']; ?>
/<?php echo $this->_tpl_vars['CURRENT_EXTRA']; ?>
/">
            <fieldset>
                <legend><h5>Add plugin to Site</h5></legend>
                <input type="file" name="plugin" id="plugin" <?php echo $_POST['plugin']; ?>
 />
                <button>Add Plugin</button>
            </fieldset>
         </form>
         
         <fieldset>
            <legend><h5>Basic Information</h5></legend>
             <div>
                <p><strong>Site Name: </strong><?php echo $this->_tpl_vars['siteDetails']->name; ?>
</strong></p>
                <p><strong>Site Alias: </strong><?php echo $this->_tpl_vars['siteDetails']->alias; ?>
</p>
                <p><strong>Site Link: </strong><a href="<?php echo $this->_tpl_vars['siteDetails']->mainSiteAddress; ?>
" target="_blank"><?php echo $this->_tpl_vars['siteDetails']->mainSiteAddress; ?>
</a></p>
                <p><strong>Other Information: </strong>Other information like color-scheme, creator etc. will come here when we link relational table.</p>
             </div>
         </fieldset>
         
         <fieldset>
            <legend><h5>Other Information</h5></legend>
             <h6>Pages created on site</h6>
             <ul>
                <li>First Page</li>
                <li>Second Page</li>
                <li>Third Page...</li>
             </ul>
         </fieldset>
         
        </div>
        
   </div>
    
    
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>