<?php /* Smarty version 2.6.26, created on 2013-05-03 15:31:34
         compiled from header.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'header.tpl', 108, false),)), $this); ?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><?php echo $this->_tpl_vars['SITE_NAME']; ?>
</title>
<!--CSS-->
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/css/reset.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/css/styles.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/css/add_css.css" type="text/css" />
<!-- Google Fonts -->
<!--<link href='http://fonts.googleapis.com/css?family=Droid+Serif:regular,italic' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:regular,bold' rel='stylesheet' type='text/css' />-->
<!--Scripts-->
<!--jQuery-->
<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/jquery.js" type="text/javascript"></script>
<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/superfish.js" type="text/javascript"></script>
<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/jquery.uniform.min.js" type="text/javascript"></script>
<link href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/css/uniform.default.css" rel="stylesheet" type="text/css">
<!--jQuery Excanvas-->
<!--[if IE]><script src="plugins/jquery-excanvas/excanvas.js" type="text/javascript"></script><![endif]-->
<!--jQuery Visualize-->
<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/visualize.jQuery.js" type="text/javascript"></script>
<link href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/visualize.css" rel="stylesheet" type="text/css">
<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/jquery.tipsy.js" type="text/javascript"></script>
<link href='<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/tipsy.css' rel='stylesheet' type='text/css' />
<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/facebox.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/css/facebox.css" type="text/css" />
<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/loader.js" type="text/javascript"></script>
<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/jquery.cookie.js" type="text/javascript"></script>
<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/styleswitch.js" type="text/javascript"></script>

<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/js/snippet.js" type="text/javascript"></script>


<link rel="stylesheet" href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/css/color1/color.css" type="text/css" title="color1" disabled />
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/css/color2/color.css" type="text/css" title="color2" disabled />
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/css/color3/color.css" type="text/css" title="color3" disabled />

<!--
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/plugins/menu/css/styles.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/plugins/menu/css/jquery-tool.css" type="text/css" />

<script type="text/javascript" src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/plugins/menu/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/plugins/menu/js/jquery.tools.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/plugins/menu/js/main.js"></script>

-->
</head>

<body class="">
<!--Header-->
<div id="header"> 
 <!--Container-->
 <div class="container clearfix"> 
  <!--Logo--> 
  <!--<img id="logo" src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/img/logo.png" alt="logo" /> -->
  <!--Menu-->
  <ul id="menu" class="clearfix">
   <li><a <?php if ($this->_tpl_vars['CURRENT_PAGE'] == '' || $this->_tpl_vars['CURRENT_PAGE'] == 'dashboard'): ?>class="current"<?php endif; ?> href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
dashboard/">Dashboard</a></li>
   <li><a href="<?php echo $this->_tpl_vars['BASE_URL']; ?>
settings/">App Settings</a></li>
   <li class="dropdown"> <a href="settings.html">Profile</a>
    <ul>
     <li><a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
create-account/">Create Account</a></li>
     <li><a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
edit-profile/">Edit Profile</a></li>
     <li><a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
logout/">Logout</a></li>
    </ul>
   </li>
  </ul>
  <!--Navigation-->
  <ul class="clearfix" id="navigation">
   
   <li <?php if ($this->_tpl_vars['CURRENT_PAGE'] == '' || $this->_tpl_vars['CURRENT_PAGE'] == 'dashboard'): ?>class="current"<?php endif; ?>><a <?php if ($this->_tpl_vars['CURRENT_PAGE'] == '' || $this->_tpl_vars['CURRENT_PAGE'] == 'dashboard'): ?>class="current"<?php endif; ?> href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
dashboard/">
	<img alt="dashboard" class="icon" src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/img/dashboard.png"/>Dashboard</a>
   </li>
   
    <li> 
		<a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
templates/">
		<img alt="layout" class="icon" src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/img/layout.png"/>Templates<span class="notification">5</span></a>
	</li> 
	
   <li>
	<a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
messages/">
		<img alt="inbox" class="icon" src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/img/inbox.png"/>Inbox
		<span class="notification">15</span>
	</a>
   	<!--<ul>
     <li><a href="#" title="">Write New</a></li>
     <li><a href="#" title="">Junk</a></li>
     <li><a href="#" title="">Deleted</a></li>
    </ul>-->
   </li>
   
   <!--<li>
    <a href="#"><img alt="colors" class="icon" src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/img/colors.png"/>Colors<span class="notification">New</span></a>
    <ul>
     <li><a href="#" rel="normal" class="styleswitch">Normal</a></li>
     <li><a href="#" rel="color1" class="styleswitch">Color 1</a></li>
     <li><a href="#" rel="color2" class="styleswitch">Color 2</a></li>
     <li><a href="#" rel="color3" class="styleswitch">Color 3</a></li>
    </ul>
   </li>-->
  
   <li <?php if ($this->_tpl_vars['CURRENT_PAGE'] == 'view-sites' || $this->_tpl_vars['CURRENT_PAGE'] == 'site-details' || $this->_tpl_vars['CURRENT_PAGE'] == 'create-page'): ?>class="current"<?php endif; ?>>
	<a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
view-sites/">
	<img alt="statistics" class="icon" src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/img/statistics.png"/>View Sites
	<span class="notification"><?php echo count($this->_tpl_vars['userSiteInfo']); ?>
</span>
	</a>
        <ul>
            <?php if (count($this->_tpl_vars['userSiteInfo']) > 0): ?>
                <li><a href="#" rel="normal" class="styleswitch">List of Sites Made Here...</a></li>
            <?php endif; ?>
            <li><a href="#create-new-site" rel="modal">Create a New Site</a></li>
        </ul>
    
   </li>
   <!--
   <li class="last">
	<a href="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
logout/"><img alt="logout" class="icon" src="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
_template/img/logout.png"/>Logout</a>
   </li>
   -->
   <li class="separator"></li>
  </ul>
  <!--end container--> 
 </div>
 <!--end #header--> 
</div>