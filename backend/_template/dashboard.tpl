{include file="header.tpl"}
<!--Content-->
<div id="content">
	<div class="container clearfix">
	
	
	<div class="box">
	   <!--Content-->
	   <div class="content padding">
			<ul class="inbox">
				 <li> 
				  <!--Avatar--> 
				  <img alt="avatar" class="avatar" src="{$BASE_URL_ADMIN}_template/img/user.png" width="40" height="40"> 
				  <!--Author-->
				  <div class="author">
					  <h4><a href="#">User</a></h4>
					  <h3 style="margin-top:5px;">{$loggedInfo->username}...</h3>
				  </div>
				  <!--Body-->
				  <div class="body">
				   <p> Welcome to {$SITE_NAME}, kick off with the list of tasks below...</p>
				  </div>
				 </li>
				 <!--separator-->
				 <li class="separator"></li>
			</ul>
	   </div>
	  </div>
	
	{include file="side-bar.tpl"}
{include file="footer.tpl"}