{include file="header.tpl"}
<!--Content-->
<div id="content">
	<div class="container clearfix">
	
	{include file="side-bar.tpl"}
    
    <div class="box three_fourth last">
 		<div class="header">
 			<h2>More Info on {$CURRENT_EXTRA|replace:'_':' '|capitalize}</h2>
            <!--Toggle-->
            <span class="toggle"></span>
 		</div>
 		<div class="content padding">
        
         {if $error}<p class="message invalid">{$error}<span class="close">X</span></p>{/if}
         {if $okay}<p class="message valid">{$okay}<span class="close">X</span></p>{/if}
         
         
         <form action="{$BASE_URL_ADMIN}{$CURRENT_PAGE}/{$CURRENT_ID}/{$CURRENT_EXTRA}/" action="post" enctype="multipart/form-data">
            <fieldset>
                <legend><h5>Add plugin to Site</h5></legend>
                <input type="file" name="plugin" id="plugin" {$smarty.post.plugin} />
                <button>Add Plugin</button>
            </fieldset>
         </form>
         
         <fieldset>
            <legend><h5>Basic Information</h5></legend>
             <div>
                <p><strong>Site Name: </strong>{$siteDetails->name}</strong></p>
                <p><strong>Site Alias: </strong>{$siteDetails->alias}</p>
                <p><strong>Site Link: </strong><a href="{$siteDetails->mainSiteAddress}" target="_blank">{$siteDetails->mainSiteAddress}</a></p>
                <p><strong>Other Information: </strong>Other information like color-scheme, creator etc. will come here when we link relational table.</p>
             </div>
         </fieldset>
         
         <fieldset>
            <legend><h5>Other Information</h5></legend>
             <h6>Pages created on site</h6>
             <ul>
                <li>First Page</li>
                <li>Second Page</li>
                <li>Third Page...</li>
             </ul>
         </fieldset>
         
        </div>
        
   </div>
    
    
{include file="footer.tpl"}