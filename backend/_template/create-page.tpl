{include file="header.tpl"}
<!--Content-->
<div id="content">
	<div class="container clearfix">
	
	{include file="side-bar.tpl"}
    
    <div class="box three_fourth last">
 		<div class="header">
 			<h2>Create Page on {$CURRENT_EXTRA|replace:'_':' '|capitalize}</h2>
    <!--Toggle-->
    <span class="toggle"></span>
 		</div>
 		<div class="content padding">
        
         {if $error}<p class="message invalid">{$error}<span class="close">X</span></p>{/if}
         {if $okay}<p class="message valid">{$okay}<span class="close">X</span></p>{/if}
         
         
         <form action="{$BASE_URL_ADMIN}create-page/{$CURRENT_ID}/{$CURRENT_EXTRA}/" method="post" id="create-page">
     <fieldset>
        <legend>Provide Page Details</legend>
        
        <div class="field">
            <label>Page Alias</label>
            <input type="text" name="page_alias" id="page_alias" class="large" {$smarty.post.page_alias} />
        </div>
        
        <input type="hidden" value="{$CURRENT_ID}" id="site_id" name="site_id" {$smarty.post.site_id} />
        
        <div class="field">
            <label>Comment: <strong><small>[optional]</small></strong></label>
            <textarea rows="7" cols="50" name="comment" id="comment" {$smarty.post.comment}></textarea>
        </div>
        
        <button type="reset" class="secondary">Reset</button>
        <button>Create Page</button>
     </fieldset>
    </form>
    {literal}
        <script type="text/javascript">
        $("#create-page").validate({
    		rules: {
    			page_alias: { required: true }
    		}
    	});
        </script>
    {/literal} 
     </div>
 	</div>
    
    
{include file="footer.tpl"}