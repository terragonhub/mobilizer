{include file="header.tpl"}
<!--Content-->
<div id="content">
	<div class="container clearfix">
	
	{include file="side-bar.tpl"}
    
    <div class="box three_fourth last">
 		<div class="header">
 			<h2>List of Sites you already created</h2>
    <!--Toggle-->
    <span class="toggle"></span>
 		</div>
 		<div class="content padding">
        
        <!--Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. -->
         {if $error}<p class="message invalid">{$error}<span class="close">X</span></p>{/if}
         {if $okay}<p class="message valid">{$okay}<span class="close">X</span></p>{/if}
         
         <div class="dataTables_wrapper">
         <table>
             <thead>
              <tr>
               <th>S/N</th>
               <th style="width:80px"></th>
               <th style="width: 120px;"> Site Name </th>
               <th class="sorting" style="width: 140px;">Alias</th>
               <th class="sorting" style="width: 160px;">Site Address</th>
               <th class="sorting" style="width: 30px;">Template</th>
               <th class="sorting" style="width: 30px;">Scheme</th>
               <th style="width:80px"></th>
              </tr>
             </thead>
             
            <tbody>
            {foreach from=$allSites item=eachSite key=eachKey}
              <tr class="gradeA odd">
                   <td>{$eachKey+1}</td>
                   <td>
                   <a href="{$BASE_URL_ADMIN}site-detials/{$eachSite->id}/{$eachSite->alias}/" title="View & Enhance {$eachSite->name}">
                        <img src="{$BASE_URL_ADMIN}_template/img/view.png" />
                    </a><a href="{$BASE_URL_ADMIN}create-page/{$eachSite->id}/{$eachSite->alias}/" title="Add Page to {$eachSite->name}">
                        <img src="{$BASE_URL_ADMIN}_template/img/edit_add.png" />
                    </a>
                   </td>
                   <td class="sorting_1">
                    <a href="{$BASE_URL_ADMIN}view-sites/{$eachSite->id}/{$eachSite->alias}/">{$eachSite->name}</a>
                    </td>
                   <td>{$eachSite->alias}</td>
                   <td>{$eachSite->mainAddress}</td>
                   <td>{$eachSite->template}</td>
                   <td>{$eachSite->colourSchemes}</td>
                   <td>
                    <a href="{$BASE_URL_ADMIN}view-sites/{$eachSite->id}/{$eachSite->alias}/"><img src="{$BASE_URL_ADMIN}_template/img/pen.png" title="Edit" /></a>
                    <a href="{$BASE_URL_ADMIN}view-sites/{$eachSite->id}/delete/" onclick="if(!confirm('Are you sure you want to delete?'))return false;"><img src="{$BASE_URL_ADMIN}_template/img/delete.png" title="Delete" /></a>
                   </td>
              </tr>
            {/foreach}
            </tbody>
      </table>
      
      <!--<div class="dataTables_info">Showing 1 to 10 of 50 entries</div>
      <div class="dataTables_paginate paging_full_numbers"><span class="first paginate_button">First</span><span class="previous paginate_button">Previous</span><span><span class="paginate_active">1</span><span class="paginate_button">2</span><span class="paginate_button">3</span><span class="paginate_button">4</span><span class="paginate_button">5</span></span><span class="next paginate_button">Next</span><span class="last paginate_button">Last</span></div>-->
      
      </div>
         
         
         
         </div>
 	</div>
    
    
{include file="footer.tpl"}