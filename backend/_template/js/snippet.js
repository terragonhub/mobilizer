function setAlias(){
    var site_name = $('#facebox #site_name').val();
    $('#facebox #site_alias').val(replaceAll(' ','_',site_name));
}

function replaceAll(find, replace, str) {
  return str.replace(new RegExp(find, 'g'), replace).toLowerCase();
}

function registerSite(){
    $('#facebox #site_name').focus();
    
    var site_name = $('#facebox #site_name');
    
	$("#facebox #create-site").validate({
		rules: {
			site_name: { required: true },
            site_template : { required: true},
            site_alias : { required:true},
            color_scheme : {required : true},
            main_address : {required:true, url:true}
		}
	});
    
    
}