<?php

/**
 * @author Oluwasegun
 * @copyright 2013
 */
 
 $siteCall = new site();
 if(!empty($_POST)){
    escape($_POST);
    $error = array();
    
    if($site_name == '') $error['site_name'] = 'Site Name Error';
    if($user_id == '') $error['user_id'] = 'User Id does not exist';
    if($site_template == '') $error['site_template'] = 'Choose a Site Template';
    if($site_alias == '') $error['site_alias'] = 'Please Set Alias';
    if($color_scheme == '') $error['color_scheme'] = 'Waiting for Color Scheme';
    if($main_address == '') $error['main_address'] = 'Main Address is needed.';
    
    if(empty($error)){
        $status = false;
        $siteCall->name = $site_name;
        $siteCall->user = $user_id;
        $siteCall->template = $site_template;
        $siteCall->alias = $site_alias;
        $siteCall->colourSchemes = $color_scheme;
        $siteCall->mainAddress = $main_address;
        try{
            $status = $siteCall->sync();
        }catch(Exception $e){
            $smarty->assign('error',$e->message());
        }
        
         if($status){
            $smarty->assign('okay','Site titled "'.$site_name.'" successfully created.'); 
         }else{
            $smarty->assign('error','Site titled "'.$site_name.'" already exist.');
         }
        
    }else{
       $smarty->assign('error','Fill all fields'); 
    } 
 }
 /*
 echo 'Page: '.$page;
 echo '<br />Id: '.$id;
 echo '<br />Extra: '.$extra;*/
 
 if(is_numeric($id) && $id != 0){
    if($extra == 'delete'){
        $site = $siteCall->load($id);
        $site->remove();
        redirect_to(BASE_URL.$page.'/');
    }
 }
 
 
 
 $allSites = $siteCall->loadByUserId($_SESSION['loggedId']);
 /*echo '<pre>';
 print_r($allSites);
 echo '</pre>';*/
 $smarty->assign('allSites',$allSites);
$template = 'view-site.tpl'

?>