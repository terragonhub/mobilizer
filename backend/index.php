<?php
/**
 * @author Oluwasegun
 * @copyright 2013
 **/
 require_once('../core/config_env.php');
 require_once('../core/_smarty/smart_url_admin.php');
    /***
    ++++++++++++++++++++++++++++++++++ REAL CODE STARTS HERE OKAY +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    **/
    $flag = 0;
    $meta_description = '';
	$meta_keywords = '';
	
	if(!isset($_SERVER['HTTP_REFERER'])) 
	{
	   $_SERVER['HTTP_REFERER'] = '';
	}
    //echo ;
    
    if(isset($_SESSION['loggedId'])){
        $userCall = new User();
       $smarty->assign('loggedInfo',$userCall->getEachUserInfo($_SESSION['loggedId']));
       $smarty->assign('userSiteInfo',$userCall->getUserSites($_SESSION['loggedId']));
    }
    
    switch($page)
	{
		case '':
            if(isset($_SESSION['loggedId'])){
                redirect_to(BASE_URL.'dashboard/');
                exit();
            }
            require_once 'page_home.php';
            $flag = 1;
		break;
        
        case 'dashboard':
            if(!isset($_SESSION['loggedId'])){
               redirect_to(BASE_URL);
                exit();
            } 
            require_once 'page_dashboard.php';
            $flag = 1;
        break;
        
        case 'view-sites':
            if(!isset($_SESSION['loggedId'])){
               redirect_to(BASE_URL);
                exit();
            } 
            require_once 'page_sites.php';
            $flag = 1;
        break;
        
        case 'site-detials':
            if(!isset($_SESSION['loggedId'])){
               redirect_to(BASE_URL);
                exit();
            } 
            require_once 'page_site_details.php';
            $flag = 1;
        break;
        
        
        case 'create-page':
            if(!isset($_SESSION['loggedId'])){
               redirect_to(BASE_URL);
                exit();
            } 
            require_once 'page_create.php';
            $flag = 1;
        break;
        
        
        case 'logout':
            if(isset($_SESSION['loggedId'])){
                unset($_SESSION['loggedId']);
                 redirect_to(BASE_URL);
                exit();
            }
            $flag = 1;
        break;                              
			
		/*case 'archives':
			require_once APP_SNIPPET.'page_archive.php';
			$flag = 1;
		break;*/
		
		// 404 etc. error page
		case 'page-unavailable':
			// TO-DO: add suggestion if no trailing slash supplied
			$html_title = 'Page unavailable / ' . SITE_NAME;
			$template = 'error.tpl';
			$flag = 1;
		break;
   }
   
   // if page not found
	if ($flag == 0){
	  // header('Location:'.BASE_URL . 'page-unavailable/', '404');
	}
    
    //SOME SMARTY CONVERSION...
    $smarty->assign('SITE_NAME',SITE_NAME);
    $smarty->assign('BASE_URL_ADMIN',BASE_URL);
    $smarty->assign('HTTP_REFERER',$_SERVER['HTTP_REFERER']);
    $smarty->assign('CURRENT_PAGE',$page);
    $smarty->assign('CURRENT_ID',$id);
    $smarty->assign('CURRENT_EXTRA',$extra);
    
    
	
	if (isset($html_title) && $html_title != '')
		$smarty->assign('html_title', $html_title);
	if (isset($meta_description) && $meta_description != '')
		$smarty->assign('meta_description', $meta_description);
	if (isset($meta_keywords) && $meta_keywords != '')
		$smarty->assign('meta_keywords', $meta_keywords);

	if (isset($template) && $template != '')
		$smarty->display($template);
    

    
 

?>