<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<meta name="author" content="Oluwasegun" />

	<title>{$SITE_NAME}</title>
</head>

<body>
    <h1>{$SITE_NAME}</h1>
    <h2>Error 404!!!</h2>
    
    <p>...page not found, go to the nearest police station.</p>
    <p>&larr;<a href="{$BASE_URL}">back to home page if you wish...</a> 
        {if $HTTP_REFERER} OR <a href="{$HTTP_REFERER}">&uarr; try previous page</a>{/if}
    </p>
    
</body>
</html>