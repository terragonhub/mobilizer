<?php
	class layoutmanager {
	
		private $page;
        private $plugins = array();
        private $template;
        
        public static $groupDataInterfaceName = "groupedDataWrapper";
        public static $singleDataInterfaceName = "unitDataWrapper";
		
		function __construct(page $page) {
			$this->page = $page;
            /// Load the plugins as soon as the page is loaded up
            $this->getPagePlugins();
            
            ///Get the page teplate
            $this->template = new template($page->site);
		}
		
		// Loads the customizations for the site like the colors and stuff	
		function loadViewCustomizations() {
			
		}
		
		
		/// Collects all the plugins that are supposed to give data to the website
		function getPagePlugins() {
		      /// Load all the plugin eneties for this page from the db
			$p = $this->page->getPlugins();
            
            /// foreach of those plugins load the instance of it from its main file
            foreach($p as $pluginEnt) {
                $pluginInstance = pluginmanager::getPluginInstance($pluginEnt->alias);
                array_push($p, $pluginInstance);
            }
		}
		
		/// Gets the data from the plugin
		function getPluginData($pluginInstance) {
			
			//$plugin->			
		}
        
        function renderPage() {
            ///Load the template
            
            ///get the data from each of the plugins and attach them
            
            foreach($this->plugins as $plugin) {
                $data = $this->getPluginData($plugin);
                $dataClassName = get_class($plugin);
                
                /// reflect the class
                $reflectionClass = new ReflectionClass($dataClassName);
                
                /// Checks if which of the data transfer interfaces the class implements
                if ($reflectionClass->implementsInterface(layoutmanager::$groupDataInterfaceName)) {
                    /// recurse through the the class 
                }elseif ($reflectionClass->implementsInterface(layoutmanager::$singleDataInterfaceName)) {
                    //// just add to the display
                }
            }
        }
		
		
		
		
		
		
		
	}
?>
