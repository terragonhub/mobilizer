<?php
    /*
        Has the proceedures for installing a new plugin
    */
    class pluginInstaller {
        // Declare the needed variables
        public $zipFile, $tempLocation, $pluginsFolder, $errors = array();
        private $pluginManifestFile, $adapter, $pluginManifestDetails;
        public static $pluginManifesFileName = "manifest.json";
        
        /// The constructor collects the location if the zip file and moves it to the temporary location for parsing
        function pluginInstaller($zipFileLocation) {
            
            
            $this->tempLocation = "temp/";
            $this->pluginsFolder = "plugins/";
            
            /////////////////////////////////////////////////////////
            $this->adapter = $GLOBALS["adapter"];
            /// Set some major variables
            $this->pluginManifestFile = $this->tempLocation.pluginInstaller::$pluginManifesFileName;
            
            if (file_exists($zipFileLocation)) {
                    //echo "found";
                $pluginZip = new ZipArchive();
                $zipRes = $pluginZip->open($zipFileLocation);
                
                //print_r($zipRes);
                $pluginZip->extractTo($this->tempLocation);
                $pluginZip->close();
            }else {
                $message = "No zip file found";
                array_push($this->errors, $message);
            }
        }
        
        /// Performs installation processes on the given plugin
        public function install() {
            $installed = false;
            /// Validate the plugin 
            if ($this->validatePlugin()) {
                 
                if (!$this->pluginExists($this->pluginManifestDetails->alias)) {
                    //// Add the plugin to the databse
                
                    /// register a new plugin
                    $pluginMapper = new plugins_mapper($this->adapter);
    
                    $pluginEnt = $pluginMapper->get();
    
                    $pluginEnt->name = $this->pluginManifestDetails->name;
                    $pluginEnt->alias = $this->pluginManifestDetails->alias;
                    $pluginEnt->created = date("y/m/d h:m:s");
                    $pluginEnt->version = $this->pluginManifestDetails->version;
                    $pluginEnt->needs_data_access = $this->pluginManifestDetails->needs_data_access;
                    $pluginEnt->needs_repititive_tasks = $this->pluginManifestDetails->needs_repititive_tasks;
                    
                    /*
                    echo '<pre>';
                    print_r($pluginEnt);
                    echo '</pre>';
                    */
    
                    if ($pluginMapper->save($pluginEnt)) {
                        /// If the plugin passes all the vlidation tests copy it to the plugins folder
                        $this->copyFolder($this->tempLocation, $this->pluginsFolder, $this->pluginManifestDetails->alias);
                        // After copying remove the temporary folder
                        @rmdir($this->tempLocation);
                        $installed = true;                    
                    }
                }else {
                    /// If the plugin exists create an error message and add it to the error list
                    $message = "Plugin ". $this->pluginManifestDetails->alias. " exists";
                    array_push($this->errors, $message);
                }
                
                
            }
            
            return $installed;
        }
        
        public function copyFolder($source, $dest, $diffDir = '') { 
            $sourceHandle = opendir($source); 
            if(!$diffDir) 
                $diffDir = $source; 
    
                mkdir($dest . '/' . $diffDir); 
    
                while($res = readdir($sourceHandle)){ 
                    if($res == '.' || $res == '..') 
                    continue; 
        
                    if(is_dir($source . '/' . $res)){ 
                        copyFolder($source . '/' . $res, $dest, $diffDir . '/' . $res); 
                    } else { 
                        copy($source . '/' . $res, $dest . '/' . $diffDir . '/' . $res); 
            
                    } 
                } 
        }
        
        
        /// Validates the plugin to see if the meets the required features for any plugin
        public function validatePlugin() {
            $pluginValid = false;
            // check to see if the manifest exists 
            if ($this->isManifestExistent()) {
                /// if the mainfest exists validate the manifest file and also the plugin file
                if ($this->validateManifestFile()) {
                    /// if that test if passed
                    $pluginValid = true;
                }
            }else {
                $message = "No mainfest file found. Please and a manifest file to your plugin";
                array_push($this->errors, $message);
            }
            
            return $pluginValid;
        }
        
        /// checks if the plugins manifest file exists in the folder
        public function isManifestExistent() {
            return file_exists($this->pluginManifestFile);
        }
        
        /// Performs all neccessary validations for the manifest file
        public function validateManifestFile() {
            $licenseFileValid = false;
            //////////////////////////////////////////////////////
            $keysOk = false; $instanceOk = false;
            
            ///////////
            $contents = file_get_contents($this->pluginManifestFile);
            
            //// check if the contents are not empty
            if (!empty($contents)) {
                /// deocde the json
                /// echo $contents;
                $pluginOptions = json_decode($contents);
                
                /*
                echo '<pre>';
                print_r($pluginOptions);
                echo '</pre>';
                */
                /// validate the licenceobject keys
                if ($this->validateManifestKeys(get_object_vars($pluginOptions))) {
                    $keysOk = true;
                    
                    /// check the instance of the plugin class actually extends the plugin interface
                    if ($this->checkPluginInstance($pluginOptions->alias)) {
                        $instanceOk = true;
                    }
                }
                
                
                /// if the the keys and the instance are okay set th valididty to true
                if ($keysOk and $instanceOk) {
                    $licenseFileValid = true;
                }
                
                /// set the manifest details
                $this->pluginManifestDetails = $pluginOptions;
                
                // return the state of things
                return $licenseFileValid;
                
            }else {
                $error = "Manifest file empty";
                array_push($this->errors, $error);
            }
        }
        
        /// Validates the license file's keys
        public function validateManifestKeys($manifest) {
            $checkOK = true;
            
            echo "<pre>";
            print_r($manifest);
            echo "</pre>";
            
            $keys = array("name", "alias", "version", "needs_data_access", "needs_repititive_tasks");
            
            foreach($keys as $key) {
                /// check if the reqired fields exists
                /// if they are not empty
                /// and also if they are not null;
                
                if ($key !=  $keys[3] && $key != $keys[4]) {
                    if (!array_key_exists($key, $manifest) || empty($manifest[$key]) || $manifest[$key] == null) {
                        $checkOK = false;
                        $message = "$key is either missing or inexistent in the manifest file";
                        array_push($this->errors, $message);
                        break;
                    }
                }
            }
            return $checkOK;
        }
        
        /// requires the plugin and checks if the plugin implements the plugin interface
        /// a test the class has to fulfill befor it can be entered into the databse
        public function checkPluginInstance($alias) {
            $checkedOk = false;
            require_once($this->tempLocation.$alias.".php");
            
            try {
                $reflectionClass = new ReflectionClass($alias);
                /// Interface implementation check
                if ($reflectionClass->implementsInterface('plugin')) {
                    $checkedOk = true;
                }
            }catch(Exception $e) {
                
            }
            
            return $checkedOk;
        }
        
        
        
        /// Checks if a given plugin exists taking its alias(namespace)
        public function pluginExists($alias) {
            /// The return variable
            $exists = false;
            /// The mapper
            $mapper = new plugins_mapper($this->adapter);
            /// Check via query
            $check = $mapper->all(array("alias" => $alias));
            
            /// any results are returned
            if (count($check) > 0) {
                $exists = true;
            }
            
            return $exists;
            
        }
        
        
        
        
    }
?>