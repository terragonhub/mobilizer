<?php
    class itemExistsException extends Exception {
        protected $message  = "Item exists in list";
        
        public function message() {
            return $this->message;
        }
    }
?>