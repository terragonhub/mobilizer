<?php
    class replacer {
        private $text = "";
        public function replacer($text) {
            $this->text = $text;
        }
        
        public function replace($txtToReplace, $replaceMent) {
            $this->text = str_replace($txtToReplace, $replaceMent, $this->text);
        }
        
        public function getText() {
            return $this->text;
        }
        
    }
?>