<?php
	/*
		Handles the plugin interation on the system
        
        @Olabode
        
        
        !!!! This class is yet to handle new plugin registration
	*/
	class pluginmanager {
	
		private $site, $adapter;
		
        /// The plugin folder
		private $pluginFolder = PLUGIN_PATH;
		
        /// An array to hold the video plugin eneties from the daatbase 
		public $plugins = array();
		
        /*
            takes the site and sets it globally to the instance
            and loads all the plugin entites
        */
		public function __construct($site) {
		     /// Set hte adapter
             
             $this->adapter = $GLOBALS["adapter"];
             /// Set the site variable
			 $this->site = $site;
            
            /// load the plugins given to the site
            $this->loadSitePlugins();
            /// verify the plugins
            $this->verifyPlugins();
		}
		
		
		// Load up all the pluguns for the requested website
		private function loadSitePlugins() {
            // Get the mapper for the table hholdin the site and plugin map
			$sitePluginMapper = new sitePluginMapper($this->adapter);
            
            /// Load the eneties from those tables
            $qPluginMapEntites = $sitePluginMapper->all(array('site' => $this->site->id));
            
            /// loop through the enetites gotten from the databse
            /// and get the plugin entity from the databse
            foreach ($qPluginMapEntites as $pluginMapEntity) {
                
                /// get the plugin mapper
                $pluginMapper = new plugins_mapper($this->adapter);
                
                /// Get the plugin enetity from the databse and push it to the plugins array
                array_push($this->plugins, $pluginMapEntity->get($pluginMapEntity->id));                
            }
		}
		
		/// Veryfies the classes to make sure they implement the plugin inteface
		/// and also return s a list of classes that dont
		public function verifyPlugins() {
			$testfailers = array();
			
			/// Loop through the plugins
			foreach($this->plugins as $plugin) {
				
				//Reflect the classs
				$reflectionClass = new ReflectionClass($plugin->alias);
				
				/// If it does not implement the plugin interface push to the failed array
				if (!$reflectionClass->implementsInterface("plugin")) {
					array_push($testfailers, $plugin);
				}
			}
			
			// return the failers
			return $testfailers;
		}
		
		
		/// Gets a plugin via its alias
		public static function getPluginInstance($alias) {
			/// check if the plugin exists in the database
			$plugin = null;
			
			$check = true;
			/// If the check for the plugin is 
			if ($check) {
				/// require the plugin class file
				require ($this->pluginFolder.$alias."/".$alias.".php");
				
				/// Reflect the class and return it
				$reflectionClass = new ReflectionClass($alias);
				
                /// check if the class implements
				if ($reflectionClass->implementsInterface('plugin')) {
					$plugin = $reflectionClass->newInstance();
				}else {
					$plugin = false;
				}
			}
			
			return $plugin;
		}
        
        /// Check the existence of the plugin in the list
        private function pluginExists($plugin) {
            $exists = false;
            foreach($this->plugins as $p) {
                if ($p->id == $plugin->id) {
                    $exists = true;
                }
            }
            return $exists;
        }
        
        
        /// adds a plugin 
        /// throws an items exists exception
        public function addPlugin($plugin) {
            $added = false;
            
            if (!$this->pluginExists($plugin)) {
                
                $sitePluginMap = new sitePluginMapper($this->adapter);
            
                $toInsert = $sitePluginMap->get();
            
                $toInsert->site = $this->site->id;
                $toInsert->plugin = $plugin->id;
            
                //print_r($toInsert);
            
                if ($sitePluginMap->save($toInsert)) {
                    $added = true;
                    array_push($this->plugins, $toInsert);
                }
            }else {
                throw new itemExistsException();
            }
            
            return $added;
        }		
	}
?>