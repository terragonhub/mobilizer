<?php
    class page {
        
        public $id = 0, $alias, $comment, $site, $entity;
        
        private $adapter;
        
        public function page($pageEntity = null) {
            $this->adapter = $GLOBALS["adapter"];
            
            if ($pageEntity instanceof phpDataMapper_Entity) {
                //echo "entity found";
                $this->alias = $pageEntity->alias;
                $this->id = $pageEntity->id;
                $this->comment = $pageEntity->comments;
                $siteCall = site::load($pageEntity->site);
                $this->site = $siteCall->load($pageEntity->site);
            }
        }
        
        
        /// Syschronises whats is present with what is availabble in db
        public function sync() {
            $insertEntity = null;
            $pagesMapper = new pages_mapper($this->adapter);
            
            if ($this->entity == null) {
                $insertEntity = $pagesMapper->get();
                
            }else {
                $insertEntity = $this->entity;
            }
            
            $insertEntity->alias = $this->alias;
            $insertEntity->comments = $this->comment;
            $insertEntity->site = $this->site;
            
            return $pagesMapper->save($insertEntity);
        }
        
        
        /// Gets the pugins for the given page
        public function getPlugins() {
            
            $plugins = array();
            
            $pagesPluginMapper = new pagesPliginMapper($this->adapter);
            $pluginMapper = new plugins_mapper($this->adapter);
            
            $pluginsPagesMaps = $pagesPluginMapper->select(array("page_id" => $this->id));
            
            foreach($pluginsPagesMaps as $pluginPagMap) {
                array_push($plugins, $pluginMapper->get($pluginPagMap->plugin_id));
            }
            
            return $plugins;
        }
        
        /// Adds a plugin for a page to execute
        public function addPlugin($pluginId) {
            
            $done = null;
            
            if ($pluginId != null) {
                $pluginPageMapper = new pagesPliginMapper($this->adapter);
                
                $mapEntity = $pluginPageMapper->get();
                
                $mapEntity->page_id = $this->id;
                $mapEntity->plugin_id = $pluginId;
                
                $done = $pluginPageMapper->save($mapEntity);
            }
            
            return $done;
        }
        
        /// Removes a plugin from a pages implementation
        public function removePlugin($pluginId) {
            $done = null;
            
            $pagesPluginMap = new pagesPluginMapper($this->adapter);
            
            $map = $pagesPluginMap->select(array("plugin_id" => $pluginId));
            
            foreach ($map as $m) {
                $done = $pagesPluginMap->delete($m);
            }
            
            return $done;
        }
        
        /// Returns all the pages for a perticualer site
        public static function getPagesFor($site) {
            
            $pages = array();
            
            //echo $site;
            $pagesMapper = new pages_mapper($GLOBALS["adapter"]);
            
            $pagesEnt = $pagesMapper->all(array("site" => $site));
            //$pagesMapper->debug();
            
            //echo count ($pagesEnt);
            
            /*
            echo '<pre>';
              echo  print_r($pagesEnt);
              echo '</pre>';
             *
             */
            
            foreach ($pagesEnt as $pageEnt) {
                array_push($pages, new page($pageEnt));
            }
           // print_r($pages);
            return $pages;
        }
        
        public function getLayoutManager() {
            
        }
    }
?>