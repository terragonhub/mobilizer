<?php

/**
 * @author Oluwasegun
 * @copyright 2013
 */

class ErrorManager{
    private $errorMessage;
    
    public function __construct($errorMessage = ''){
        if($errorMessage != ''){
            //log error message....
            $this->showError($errorMessage);
        }
        $this->errorMessage = $errorMessage;
        exit();
    }
    
    private function showError($e){
        //global $adapter;
        echo $e;
    }
    
    
    
}

?>