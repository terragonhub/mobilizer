<?php

    /**
     * @author Oluwasegun
     * @copyright 2013
     */
     
     // Setup Smarty
	$smarty = new Smarty();
	$smarty->template_dir = APP_PATH .DIRECTORY_SEPARATOR.'views/_templates' . DIRECTORY_SEPARATOR . THEME . DIRECTORY_SEPARATOR;
	$smarty->compile_dir = APP_PATH .DIRECTORY_SEPARATOR.'views/_templates' . DIRECTORY_SEPARATOR . THEME . DIRECTORY_SEPARATOR . '_cache';
    
    $scheme = "http";
     
     define('APP_URL',$scheme.'://'.$_SERVER['HTTP_HOST'].APP_MAIN_DIR.'/');
    
    // Split URL - get parameters
    
	$_app_info['params'] = array();
	
	if (isset($_SERVER['HTTP_X_ORIGINAL_URL']))
	{
		$_SERVER['REQUEST_URI'] = $_SERVER['HTTP_X_ORIGINAL_URL'];
	}
	if (isset($_SERVER['HTTP_X_REWRITE_URL']))
	{
		$_SERVER['REQUEST_URI'] = $_SERVER['HTTP_X_REWRITE_URL'];
	}
    //Where real stuff is set....
	$newUrl = str_replace('/', '\/', APP_MAIN_DIR);
    $pattern = '/'.$newUrl.'/';   
    $_url = preg_replace($pattern, '', $_SERVER['REQUEST_URI'], 1);
	$_tmp = explode('?', $_url);
	$_url = $_tmp[0];	
	
	if ($_url = explode('/', $_url))
	{
		foreach ($_url as $tag)
		{
			if ($tag)
			{
				$_app_info['params'][] = $tag;
			}
		}
	}
    
    $site_name = (isset($_app_info['params'][0]) ? mysql_real_escape_string($_app_info['params'][0]) : '');
	$plugin = (isset($_app_info['params'][1]) ? mysql_real_escape_string($_app_info['params'][1]) : '');
	$data = (isset($_app_info['params'][2]) ? mysql_real_escape_string($_app_info['params'][2]) : '');
	$identity = (isset($_app_info['params'][3]) ? mysql_real_escape_string($_app_info['params'][3]) : '');
    $more = (isset($_app_info['params'][4]) ? mysql_real_escape_string($_app_info['params'][4]) : '');
    //you can add more to it based on the structure of the url
?>