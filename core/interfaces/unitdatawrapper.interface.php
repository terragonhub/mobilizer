<?php
	interface unitDataWrapper {
		 
		// gets any link that is associated with the plugin
		public function getLink();
		
		// anyimage associated with the data
		public function getImage();
		
		// Turns the object ot a string
		public function __toString();
		
		// function to tell the display manager how to display the data
        // 1 - represents exerpt mode
        // 2 - represents full mode
		public function displayMode();
                
                /// gets the title
                
                public function getTitle();
	}
?>