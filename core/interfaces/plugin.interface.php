<?php
	interface plugin {
		
		/// Method the initializes the plugin
		public function init($s);
		
		/// Gets gets the data associated with a particular site
		public function getData();
		
		/// returns a path to the confing interface where the plugin varibles can be customised
		public function getConfigInterface();
		
		/// return information about the plugin
		public function getInfo();
	}
?>
