<?php
	interface groupedDataWrapper {
		
		// returns all the unit wrappers contained in this wrapper
		public function getData();
		
		// returns the data alias for the group of data
		public function getGroupDataAlias();
		
		// gets the wrapper object at a given index
		public function get($index);
		
		// Removes a wrapper object from the pack
		public function remove($object);
		
		// Adds an object to the pack
		public function add($object);
        
                /// Gets the number of unit data objects present in this class
                public function count();
        
                //Gets the starting index for the data 
                public function startingIndex();
                
                public function getLink();
	}
?>
