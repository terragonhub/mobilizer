<?php
    // Load the replacer.pnph file
    /*
        the template class
    */
    class template {
        public $site, $template, $templatesDir = VIEWS_PATH;
        
        /// The private properties
        private $manifestDetails, $mainHTML, /*Used in exerpt mode */ $unitWrapperHTML, $groupDataWrapperHTML, $itemHTML, /*Used in extended mode */ $unitWrapprerHTMLExtended;
        
        
        public static $manifestFileName = "manifest.json";
        
        private $adapter;
        
        /// The constructor takes the site as an arguement
        public function template($site) {
            $this->adapter = $GLOBALS["adapter"];
            $this->site = $site;
            $this->smarty = $smarty = $GLOBALS["smarty"];
            
            /// get the entity from the db
            $this->getTemplateEntity();
            
            /// Get the templates files from the file system
            $this->getTemplateFiles();
            
            /// Ge the relevant HTML
            
            $this->loadHTMLChunks();
        }
        
        /// gets the template infor from the database
        private function getTemplateEntity() {
            $templateMappper = new template_mapper($this->adapter);
            $this->template = $templateMappper->get($this->site->template);
        }
        
        /// Loads all the relevant template files from the templates foldder
        private function getTemplateFiles() {
            $templateFilesPath = $this->templatesDir.DIRECTORY_SEPARATOR.$this->template->alias;
            
            /// The path to the manifest file
            $mainfestFilePath = $templateFilesPath.DIRECTORY_SEPARATOR.template::$manifestFileName;
            
            if (file_exists($mainfestFilePath)) {
                /// if the manifest file has been found set the manifest details
                $this->setManifestDetails(file_get_contents($mainfestFilePath));
            }else {
                die ("Manifest file not found");
            }
        }
        
        
        /// Load the templates manifest details from the filesystem and parse them
        private function setManifestDetails($jsonText) {
            //$fields = array("mainLayout", "groupedData", "unitData", "unitDataExtended");
            $this->manifestDetails = json_decode($jsonText);
        }
        
        /// Loads the various html chunck s for the mainhtml file the grouped data file and the unit data file
        private function loadHTMLChunks() {
            /// Set the file path to the files for the template
            $templateFilesPath = $this->templatesDir.DIRECTORY_SEPARATOR.$this->template->alias;
            
            if ($this->manifestDetails != null) {
                /// Load the html files for the this template into memory for manupulation
                $this->mainHTML = file_get_contents($templateFilesPath.DIRECTORY_SEPARATOR.$this->manifestDetails->mainLayout);
                $this->groupDataWrapperHTML = file_get_contents($templateFilesPath.DIRECTORY_SEPARATOR.$this->manifestDetails->groupedData);
                $this->unitWrapperHTML = file_get_contents($templateFilesPath.DIRECTORY_SEPARATOR.$this->manifestDetails->unitData);
                $this->itemHTML = file_get_contents($templateFilesPath.DIRECTORY_SEPARATOR.$this->manifestDetails->itemsHTML);
                $this->unitWrapprerHTMLExtended = file_get_contents($templateFilesPath.DIRECTORY_SEPARATOR.$this->manifestDetails->unitDataExtended);
            }else {
                die("template manifest details have not been set");
            }
        }
        
        
        /// The action functions
        public function setPageTitle($title) {
            /// smarty
            /// Replace the [title] tag
            
            /// ref variable: $this->mainHTML
            
            $replacer = new replacer($this->mainHTML);
            $replacer->replace("[title]", $title);
            
            $this->mainHTML = $replacer->getText();
        }
        
        public function setPageLogo($logoUrl) {
            /// smarty
            /// Replace the [logo] tag
            
            /// ref variable: $this->mainHTML
            
            $replacer = new replacer($this->mainHTML);
            $replacer->replace("[logo]", $title);
            
            $this->mainHTML = $replacer->getText();
        }
        
        
        public function setPageFooterText() {
            /// smarty
            /// Replace the [footer] tag
            
            /// ref variable: $this->mainHTML
            
            $replacer = new replacer($this->mainHTML);
            $replacer->replace("[footer]", $title);
            
            $this->mainHTML = $replacer->getText();
        }
        
        /*
            There are still ther customization functions that can come in here the makt this more robust
            this what i remember for now, There should still be functions for changin the link colour and stuff
        
        */
        
        
        /// This grouped data class coming from the plugin can be named anything but it must implement the groupedDataWrapper interface
        /// to make sure that some functions are availabe to the core to call
        public function addGroupedData($groupedDataClass) {
            /// Ref variable: $this->groupDataWrapperHTML
            /// create a temo variable to carry the value of $this->groupDataWrapperHTML
            
            /// Step one 
            /// Repalce the title tag [title] on the grouped data widget
            /// call the getGroupDataAlias() method on the class
            
            /// Loop throught the data presnet
            /// get the index of the first of the data by using the startingIndex()
            
            /// Loop through with the starting index found using the get($index)
            /// the get($index) method should return a class implementing the the unitdata interface
            ///Use smarty to lopp through the list sgments
            /// apend the link for more from the 
            
            /// whe the html is done loading apennds it the the main HTML via repalcing the content tag
            /// and readd the [content] so another apendation can bedone
            
            
            $temp = $this->groupDataWrapperHTML;
            
            /// replace the tile
            $temp = str_replace("[title]", $groupedDataClass->getGroupDataAlias(), $temp);
            
            // Get the starting index
            $x = $groupedDataClass->startingIndex();
            /// Get the amount of data in the group
            $length = $groupedDataClass->count();
            /// Once the stating index and the length has been gotten loop thorugh
            $items = "";
            while($x < $length) {
                $itemTemp = $this->itemHTML;
                $data = $groupedDataClass->get($x);
                $itemTemp = str_replace("[item_string]", $data->getTitle(), $itemTemp);
                $itemTemp = str_replace("[image_href]", $data->getImage(), $itemTemp);
                /// Add it to the items HTML
                $items .= $itemTemp;
                $x++;
            }
            
            /// When the HTMl from the Items have been gotten, replace the [content] place holder
            // of the group temp
            
            /// replace the tile
            $temp = str_replace("[content]", $items, $temp);
            
            /// replace its link
            $temp = str_replace("[link]", $groupedDataClass->getLink(), $temp);
            
            /// Add the content to the main html
            $this->addContent($temp);
        }
        
        /// Adds content the main html
        private function addContent($contentHTML) {
            $this->mainHTML = str_replace("[content]", $contentHTML."[content]", $this->mainHTML);
        }


        /// Add just a chunk of dats to the view
        public function addUnitData($unitDataClass) {
            /// Ref variable:$this->unitWrapperHTML
            
            /// check the display mode using the displayMode() method
            
            /// if the display mode is one(exerpt mode)
            /// use the current unitWrapperHTML;
            /// else use the unitDataExtended
            
            //for either case
            // replace the [title] tag
            // the [link_url] tag
            
            //Use smarty to append to the main HTML and add the [content] tag back
            
            function doReplace($html) {
                $temp = $html;
                $temp = str_replace("[title]", $unitDataClass->getTitle(), $temp);
                $temp = str_replace("[link]", $unitDataClass->getLink(), $temp);
                $temp = str_replace("[content]", $unitDataClass, $temp);
                
                return $temp;
            }
            
            /// If in exerpt mode
            if ($unitDataClass->displayMode() == 1) {
                $toAdd = doReplace($this->unitWrapperHTML);
                $this->addContent($toAdd);
                /// If in extended mode
            }elseif($unitDataClass->displayMode() == 2) {
                $toAdd = doReplace($this->unitWrapprerHTMLExtended);
                $this->addContent($toAdd);
            }
        }
        
        public function render() {
            $this->mainHTML = str_replace("[content]", "", $this->mainHTML);
            
            return $this->mainHTML;
        }
        
        
        
    }
?>