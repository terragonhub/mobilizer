<?php

/**
 * @author Oluwasegun
 * @copyright 2013
 */

class User{
    var $id,$username,$email,$type,$password;
    var $userMapperCall;
    
    public function __construct(){
        global $adapter;
        //$adapter = $_GLOBAL['adapter'];
        $this->userMapperCall = new userMapper($adapter);
        
        //if possible set parameters here...
    }
    
    public function getAllUsers($type = false){ //returns array format.
        // all users info with a status of 'published'
        $allUsers = array();
        if(is_numeric($type))
            $allUsers = $this->userMapperCall->all(array('type'=>$type))->order(array('created_on'=>'DESC'));
        else
            $allUsers = $this->userMapperCall->all()->order(array('created_on'=>'DESC'));
        
        return $allUsers;
    }
    
    public function getEachUserInfo($id){ //returns array
        return $eachUser = $this->userMapperCall->get($id);
    }
    
    private function checkUserInfo($username,$email){ //returns true or false
        $exist = false;
        $check = $this->userMapperCall->select()
                            ->from($this->userMapperCall->getDataSource())
                            ->where(array('username' => $username))
                            ->orWhere(array('email' => $email));
        if( count($check) > 0)
            $exist = true;
        else
            $exist = false;
            
        return $exist;
    }
    
    public function createUser($param){ //returns false or the inserted Id...
        $status = false;
        if($this->checkUserInfo($param['username'],$param['email']))
            $status =  false;
        else{
            $creation = $this->userMapperCall->get();
            $securityCall = new Security_Authentication();
            # Set data and save it
            $creation->username = $param['username'];
            $creation->email = $param['email'];
            $creation->type = $param['type'];
            $creation->password = $securityCall->ashPassword($param['password']);
            $creation->created_on = date("Y-m-d h:i:s");
            $this->userMapperCall->save($creation);
            $status =  $creation->id;
        }
        return $status;
    }
    
    public function updateUser($param){ //returns the updated user id
        $update = $this->userMapperCall->get();
        $securityCall = new Security_Authentication();
        # Set data and save it
        $update->username = $param['username'];
        $update->email = $param['email'];
        $update->type = $param['type'];
        $update->password = $securityCall->ashPassword($param['password']);
        $update->id = $param['id'];
        $status = $this->userMapperCall->update($update);
        return $update->id;
    }
    
    public function authenticateUser($username_or_email,$password){ //returns false or the user identity (id)
        $status = false;
        $securityCall = new Security_Authentication();
        $checked = $this->userMapperCall
                ->query(
                'SELECT id 
                FROM '.$this->userMapperCall->getDataSource().' 
                WHERE ( username = :username_or_email OR email = :username_or_email ) 
                AND password = :password', 
                array('username_or_email'=>$username_or_email,'password'=>$securityCall->ashPassword($password))
                );
                
        if($checked->count() > 0)
            $status = $checked->current()->id;
        else
           $status = false;
        
        return $status;      
    }
    
    
    public function deleteUser($user_id){
        $delete = $this->userMapperCall->get();
        $delete->id = $user_id;
        $this->userMapperCall->delete($delete);
    }
    
    public function getUserSites($id){ //returns array of sites_id or null
        $allSites = null;
        global $adapter;
        $userSiteMapperCall = new userSiteMapper($adapter);
        $check_site = $userSiteMapperCall
                ->query(
                'SELECT site FROM
                '.$userSiteMapperCall->getDataSource().' 
                WHERE user = :user_identity', array('user_identity'=>$id)
                );
                
        if($check_site->count() > 0)
            $allSites = $check_site;
            
        return $allSites;
    }
    
    
    
    
}

?>