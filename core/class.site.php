<?php
	class site {
		public $id, $name, $user, $template, $alias, $mainAddress, $colourSchemes,  $entity = null, $admins=array();
        
        /// The table name
        public static $tableName = "mblzr_sites";
        
        ///
        private $adapter; 
        
        /*
            This class can take the entity for the the site table and use it to set the class up
        */
		function __construct($entity = null) {
		      $this->adapter = $GLOBALS["adapter"];
              
              // if the arguement is not null and is a databse entiy set it the the class properties
			if ($entity != null && $entity instanceof phpDataMapper_Entity) {
			     $this->id = $entity->id;
                             $this->name = $entity->name;
                 $this->user = $entity->user;
                 $this->template = $entity->template;
                 $this->alias = $entity->alias;
                 $this->mainAddress = $entity->mainSiteAddress;
                 $this->colourSchemes = $entity->colourSchemes;
                 $this->entity = $entity;
			}
		}
        
        
        
        /*
            inserts the data in this instance of the class to a new row in the db if it isnt existent
            and updates it if it already exists
        */
        public function sync() {
            
            $insertEntity = null;
            $result = false;
            /// The major checking property is the entity property of the instance
            /// if it is set then that means the record available in the class is gotten form the databse
            /// else it is insert\
            
            if ($this->entity == null) {
                $site_mapper = new site_mapper($this->adapter);
                $insertEntity = $site_mapper->get();
                
                /// Cehck if a website with the same alias already exists
                $qResult = $site_mapper->all(array('alias' => $this->alias));
                
                if (count($qResult) > 0) {
                    /// Throw an exception if the site already exists
                    throw new itemExistsException();
                }
                
            }elseif ($this->entity instanceof phpDataMapper_Entity) {
                $insertEntity = $this->entity;
            }
            
            $insertEntity->name = $this->name;
            $insertEntity->user = $this->user;
            $insertEntity->template = $this->template;
            $insertEntity->alias = $this->alias;
            $insertEntity->mainSiteAddress = $this->mainAddress;
            $insertEntity->colourSchemes = $this->colourSchemes;
            
            /*
            echo '<pre>';
            print_r($insertEntity);
            echo '</pre>';
            */  
            
            try {
                $result = $site_mapper->save($insertEntity);                
            }catch(Exception $e) {
                $e->getMessage();
            }
            
            $this->id = $insertEntity->id;
            
            return $result;
        }
        
        
        /// Loads a specific class from the databsse
        public static function load($id) {
            /// load up the site mapper
            $siteMapper = new site_mapper($GLOBALS["adapter"]);
            
            /// get the row entity
            $entity = $siteMapper->get($id);
            //print_r($entity);
            
            /// return the site object
            return (new site($entity));
        }
        
        
        /// Load the the plugin nanger object for this instance
        public function getPluginManager() {
            $pluginManager =  new pluginmanager($this);
            return $pluginManager;
        }
        
        /// Loads a website by its alias
        public function loadByAlias($alias) {
            /// load the site mapper
            $siteMapper = new site_mapper($this->adapter);
            
            /// to save the entity gotten
            $siteEntity = null;            
            $qSiteList = $siteMapper->all(array('alias' => $alias));
            
            /// becouse the result comes as a collection we move through it via foreach
            foreach ($qSiteList as $sitent) {
                $siteEntity = $sitent;
            }
            
            /// create the site variable to hole the site object if data was return from the databse
            $site = null;
            
            /// check if nothing was gotten             
            if ($siteEntity != null) {
                /// if theres was data create the object from the entity
                $site = new site($siteEntity);
            }
            
            // return the data;
            return site;
        }
        
        
        /// Loads a website by its user id
        public function loadByUserId($user_id) {
            $sites = array();
            /// load the site mapper
            $siteMapper = new site_mapper($this->adapter);
            
            /// to save the entity gotten
            $siteEntity = null;            
            $qSiteList = $siteMapper->all(array('user' => $user_id));
            
            /// becouse the result comes as a collection we move through it via foreach
            foreach ($qSiteList as $sitent) {
                array_push($sites, new site($sitent));
            }
            
            // return the data;
            return $sites;
        }
        
         /// Loads site information
        public function loadBySiteId($site_id) {
            $site = array();
            /// load the site mapper
            $siteMapper = new site_mapper($this->adapter);
            
            /// to save the entity gotten
            $siteEntity = null;  
            $site =  $siteMapper->get($site_id);       
            /*$qSiteList = $siteMapper->all(array('id' => $site_id));
            
            /// becouse the result comes as a collection we move through it via foreach
            foreach ($qSiteList as $sitent) {
                array_push($sites, new site($sitent));
            }*/
            
            // return the data;
            return $site;
        }
        
        
        /// Removes a site form exixstence
        public function remove() {
            $siteMapper = new site_mapper($this->adapter);
            $siteMapper->delete($this->entity);
        }
        
        
        /// adds a new user as an admin to the site
        public function addAdmin($user) {
            $siteAdminMapper = new siteAdminMapper($this->adapter);
            
            $insertEntity = $siteAdminMapper->get();
            
            $insertEntity->user = $user->id;
            $insertEntity->site = $this->id;
            
            
        }
        
        /// Removes a user from being an admin for a website
        public function removeAdmin($user) {
            /// removed
            $removed = false;
            /// create the mapper
            $siteAdminMapper = new siteAdminMapper($this->adapter);
            
            
            $qResult = $siteAdminMapper->all(array('user' => $user->id, 'site' => $this->id));
            
            /// if the query returns results results it is goin to be one
            if (count($qResult) > 0) {
                $removed = $siteAdminMapper->delete($qResult[0]);
            }
            
            return $removed;
        }
        
        
	}
	
?>