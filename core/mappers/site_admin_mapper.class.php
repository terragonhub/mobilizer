<?php
    class siteAdminMapper extends phpDataMapper_Base {
        protected $_datasource = "mblzr_site_admin_map";
        
        public $id = array ('primary' => true, 'serial' => true, 'type' => 'int');
        public $site = array ('type' => 'int', 'required' => true);
        public $user = array ('type' => 'int', 'required' => true);
    }
    
    $siteAdminMapper = new siteAdminMapper($adapter);
    $siteAdminMapper->migrate();
?>