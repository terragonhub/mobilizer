<?php
     /*
        This class basically contains mapper definations fot the plugins table in the database
    */
    class sitePluginMapper extends phpDataMapper_Base {
        protected $_datasource = "mblzr_site_plugin_map";
        
        public $id = array('primary'=> true, 'type'=>'int', 'serial'=>true);
        public $site = array ('type' => 'int', 'required' => true);
        public $plugin = array ('type' => 'int', 'required' => true);
    }
    
    $sitePluginMapper = new sitePluginMapper($adapter);
    $sitePluginMapper->migrate();
?>