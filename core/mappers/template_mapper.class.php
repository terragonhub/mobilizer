<?php
    class template_mapper extends phpDataMapper_Base {
        protected $_datasource = "mblzr_templates";
        
        public $id = array ("type" => "int", "serial" => true, "primary" => true);
        public $name = array ("type" => "string", "required" => true);
        public $alias = array ("type" => "string", "required" => true);
        public $created = array ("type" => "datetime", "required" => true);
    }
    
    $templateMapper = new template_mapper($adapter);
    $templateMapper->migrate();
?>