<?php
    /*
        This class basically contains mapper definations fot the plugins table in the database
    */
    class plugins_mapper extends phpDatamapper_Base {
        
        protected $_datasource = "mblzr_plugins";
        
        public $id = array('type'=> 'int', 'primary' => true, 'serial' => true);
        public $name = array ('type'=>'string', 'required'=>true);
        public $alias = array('type'=>'string', 'required'=>true);
        public $created = array ('type' => 'datetime', 'required' => true);
        public $version = array ('type' => 'int', 'required' => true);
        public $needs_data_access = array ('type'=>'int');
        public $needs_repititive_tasks = array('type' => 'int');
        
    }
    
    $pluginMapper = new plugins_mapper($adapter);
    $pluginMapper->migrate();
?>