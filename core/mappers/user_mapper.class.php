<?php

/**
 * @author Oluwasegun
 * @copyright 2013
 */

// User
class userMapper extends phpDataMapper_Base
{
    // Specify the data source (table for SQL adapters)
    protected $_datasource = "mblzr_users";
    // Define your fields as public class properties
    public $id = array('type' => 'int', 'primary' => true, 'serial' => true);
    public $username = array('type' => 'string', 'required' => true);
    public $email = array('type' => 'string', 'required' => true);
    public $type = array('type' => 'int', 'default' => 0);
    public $password = array('type' => 'string','required'=>true);
    public $created_on = array('type'=>'datetime');
    
    // Comments relationship
    public $sites = array(
        'type' => 'relation',
        'relation' => 'HasMany',
        'mapper' => 'userSiteMapper',
        'where' => array('user' => 'entity.id')
        // Means adminUserMapper.user = currently loaded User entity id
    );
    
    public function getDataSource(){
        return $this->_datasource;
    }
    
    
}

global $adapter;
$userMapperCall = new userMapper($adapter);
$userMapperCall->migrate();

?>