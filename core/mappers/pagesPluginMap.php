<?php
    class pagesPluginMapper extends phpDataMapper_Base {
        
        protected $_datasource = "mblzr_pages_plugins_map";
        
        public $id = array("type" => "int", "serial" => true, "primary" => true);
        public $page_id = array("type" => "int", "required" => true);
        public $plugin_id = array("type" => "int", "required" => true);
    }
    global $adapter;
    $pagesPluginMapper = new pagesPluginMapper($adapter);
    $pagesPluginMapper->migrate();
?>