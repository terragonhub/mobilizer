<?php
     /*
        This class basically contains mapper definations fot the plugins table in the database
    */
    class site_mapper extends phpDataMapper_Base {
        
        protected $_datasource = "mblzr_sites";
        
        public $id = array('primary'=> true, 'type'=>'int', 'serial'=>true);
        public $user = array ('type'=>'int', 'required'=>true);
        public $colourSchemes = array ('type' => 'int'); /// dont have required attributes because this cant be given on site registration
        public $template = array('type' => 'int'); /// dont have required attributes because this cant be given on site registration
        public $alias = array ('type'=>'string', 'required'=>true);
        public $name = array ('type' => 'string', 'required' => true);
        public $mainSiteAddress = array('type' => 'string', 'required' => true);
    }
    
    $siteMapper = new site_mapper($adapter);
    $siteMapper->migrate();
?>