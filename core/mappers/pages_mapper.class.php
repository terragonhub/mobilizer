<?php
    class pages_mapper extends phpDataMapper_Base {
        protected $_datasource = "mblzr_pages";
        
        public $id = array("type"=>'int', "serial" =>true, "primary" =>true);
        public $alias = array ("type" => "string", "required" => true);
        public $comments = array ("type" => "string");
        public $site = array("type" => "int", "required" => true);
    }
    
    $pagesMapper = new pages_mapper($adapter);
    
    
    $pagesMapper->migrate();
?>