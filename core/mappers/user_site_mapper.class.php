<?php

/**
 * @author Oluwasegun
 * @copyright 2013
 */

// userSite Mapper
class userSiteMapper extends phpDataMapper_Base
{
    // Specify the data source
    protected $_datasource = "mblzr_user_site";
 
    // Define your fields as public properties
    public $id = array('type' => 'int', 'primary' => true, 'serial' => true);
    public $site = array('type' => 'int', 'required' => true);
    public $user = array('type' => 'int', 'required' => true);
    
    
     public function getDataSource(){
        return $this->_datasource;
    }
    
}

global $adapter;
$userSiteMapperCall = new userSiteMapper($adapter);
$userSiteMapperCall->migrate();

?>