<?php

/**
 * @author Oluwasegun
 * @copyright 2013
 */
// Constants generated for Mobilizer..

    define('ASH_METHOD',1); // 1-md5, 2-sha1, 3-sha2, 4-md6...it should be noted that the default is md5
    define('APP_PATH', str_replace(DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, str_replace('core', '',dirname(__FILE__)) . DIRECTORY_SEPARATOR));
    define('CORE_PATH',APP_PATH.'core');
    define('ROOT_DIR',basename(__DIR__));	
	define('APP_MAIN_DIR', rtrim(dirname($_SERVER['SCRIPT_NAME']),'/\\'));
    define('PLUGIN_PATH',APP_PATH.'plugins');
    define('APP_SNIPPET',APP_PATH.'/views/snippets/');

// Database configurations....
    define('DB_SERVER','localhost');//database host name - prefered localhost;
    define('DB_HOST','root');//database host name - prefered root;
    define('DB_NAME','mobilizer'); //database name - prefered mobilizer;
    define('DB_PASSWORD',''); //database password - prefered '';
    
    //more define constants can be added here...
    define ('THEME','bodSegz');
    define ('SITE_NAME','Mobilizer 1.0');
    
    // Require base mapper
    require_once(CORE_PATH.'/phpDataMapper/Base.php');
    // Connecting to Database
    require (CORE_PATH.'/phpDataMapper/Adapter/Mysql.php');
    
    
    
    //Now require all the necessary core files....
    require_once(CORE_PATH.'/class.ErrorManager.php');
    require_once(CORE_PATH.'/class.functions.php');
    
    require_once(CORE_PATH.'/class.layoutmanager.php');
    require_once(CORE_PATH.'/class.page.php');
    require_once(CORE_PATH.'/class.plugin_installer.php');
    require_once(CORE_PATH.'/class.replacer.php');
    require_once(CORE_PATH.'/class.Security_Authentication.php');
    require_once(CORE_PATH.'/class.User.php');
    require_once(CORE_PATH.'/class.site.php');
    require_once(CORE_PATH.'/class.page.php');
    require_once(CORE_PATH.'/class.template.php');
    require_once(CORE_PATH.'/class.zip.php');
    require_once(CORE_PATH.'/itemExists.exception.php');
    
    require_once (CORE_PATH.'/smarty/libs/Smarty.class.php');
    
    try {
        $adapter = new phpDataMapper_Adapter_Mysql(DB_SERVER, DB_NAME, DB_HOST, DB_PASSWORD);
        $GLOBALS["adapter"] = $adapter;
    } catch(Exception $e) {
        //echo $e->getMessage();
        $errorCall = new ErrorManager($e->getMessage());
        exit();
    }
    
    
    
    //Require all Mappers
    requireFilesInFolder(CORE_PATH.'/mappers');
    /*require_once(CORE_PATH.'/mappers/user_mapper.class.php');
    require_once(CORE_PATH.'/mappers/user_site_mapper.class.php');
    require_once(CORE_PATH.'/mappers/pages_mapper.class.php');
    require_once(CORE_PATH.'/mappers/pagesPluginMap.php');
    require_once(CORE_PATH.'/mappers/plugins_mapper.class.php');
    require_once(CORE_PATH.'/mappers/site_admin_mapper.class.php');
    require_once(CORE_PATH.'/mappers/site_mapper.class.php');
    require_once(CORE_PATH.'/mappers/site_plugin_map.class.php');
    require_once(CORE_PATH.'/mappers/template_mapper.class.php');
    require_once(CORE_PATH.'/mappers/user_mapper.class.php');
    require_once(CORE_PATH.'/mappers/user_site_mapper.class.php');*/
    
    
    
     require_once('_smarty/smart_url.php');
    
    session_start();
    

?>