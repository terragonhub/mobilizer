<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of template_installer
 *
 * @author Olabode
 */
class template_installer {
    private $_url, $tempLocation, $tempFolderName;
    public $error = array();
    public static $_templateManifestFIleName = "manifest.json";
    
    public function __construct($url) {
        /// Set the url
        $this->_url = $url;
        /// Set the temp location
        $this->tempLocation = "temp/";
        
        /// Extract the zip archive to plugin 
        if (file_exists($url)) {
            $zip = new ZipArchive();
            $zip->open($url);
            
            $this->tempFolderName = $this->generateRandName();
            
            $zip->extractTo($this->tempLocation.$this->tempFolderName);
            $zip->close();
        }else {
            $this->logError("Zip file not found");
        }
    }
    
    /// Starts the installation process
    public function install() {
        
        $installed = false;
        
        $manifestFileContent = $this->loadManifestFile();
        
        if ($manifestFileContent != "") {
            if ($this->checkManifestVars($manifestFileContent)) {
                $installed = true;
            }
        }
        
        return $installed;
    }


    /// Generates a randon name for the file
    private function generateRandName() {
        $filename = NULL;
        do {
            $number = rand(0, 1000000);
            $filename = sha1($number);
        }while(file_exists($this->tempLocation.$filename));
        
        return $filename;
    }
    
    
    /// Loads out all the conetent of the manifest file for json parsing 
    private function loadManifestFile() {
        $manifestFile = $this->tempLocation.$this->tempFolderName."/".template_installer::$_templateManifestFIleName;
        
        $content = "";
        
        //echo $manifestFile;
        /// Check if manifest file exsitsa and load its content
        if (file_exists($manifestFile)) {
            $content = file_get_contents($manifestFile);
        }else {
            $this->logError("No manifest file was found");
        }
        
        return $content;
    }
    
    /// Checks the manifest to make sure that the required variables are present
    private function checkManifestVars($manifestText) {
        $requiredKeys = array("main_html", "grouped_data_html", "unit_data_html", "item_html", "unit_data_html_extended");
        $folder = $this->tempLocation.$this->tempFolderName;
        
        $manifestObj = json_decode($manifestText);
        $manifestArray = get_object_vars($manifestObj);
        
        print_r($manifestArray);
        $check = true;
        
        foreach ($requiredKeys as $requiredKey) {
            if (!array_key_exists($requiredKey, $manifestArray) && $check) {
                $check = false;
                $this->logError($requiredKey." is not found in the manifest file");
                break;
            }
            
            $keyFileName = $folder."/".$manifestArray[$requiredKey];
            
            if (!file_exists($keyFileName) and $check) {
                $check = false;
                $this->logError("$keyFileName as not found");
                break;
            }
        }
        
        return $check;
    }
    
    /// Pushes error messages to the $this-> erro array
    private function logError($message) {
        array_push($this->error, $message);
    }
    
    
}

?>
