<?php
/**
 * @author Oluwasegun
 * @copyright 2013
 **/
 require_once('core/config_env.php');
 //require_once('core/_smarty/smart_url.php');
    /***
    ++++++++++++++++++++++++++++++++++ REAL CODE STARTS HERE OKAY +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    **/
    $flag = 0;
    $meta_description = '';
	$meta_keywords = '';
    
    define('BASE_URL',APP_URL);
	
	if(!isset($_SERVER['HTTP_REFERER'])) 
	{
	   $_SERVER['HTTP_REFERER'] = '';
	}
    
    switch($site_name)
    {
		case '':
			require_once APP_SNIPPET.'mob_home.php';
			$flag = 1;
		break;
			
		/*case 'archives':
			require_once APP_SNIPPET.'page_archive.php';
			$flag = 1;
		break;*/
		
		// 404 etc. error page
		case 'page-unavailable':
			// TO-DO: add suggestion if no trailing slash supplied
			$html_title = 'Page unavailable / ' . SITE_NAME;
			$template = 'error.tpl';
			$flag = 1;
		break;
   }
   
   // if page not found
	if ($flag == 0){
	   header('Location:'.BASE_URL . 'page-unavailable/', '404');
	}
    
    //SOME SMARTY CONVERSION...
    $smarty->assign('SITE_NAME',SITE_NAME);
    $smarty->assign('BASE_URL', BASE_URL);
    $smarty->assign('HTTP_REFERER', $_SERVER['HTTP_REFERER']);
    
    
	
	if (isset($html_title) && $html_title != '')
		$smarty->assign('html_title', $html_title);
	if (isset($meta_description) && $meta_description != '')
		$smarty->assign('meta_description', $meta_description);
	if (isset($meta_keywords) && $meta_keywords != '')
		$smarty->assign('meta_keywords', $meta_keywords);

	if (isset($template) && $template != '')
		$smarty->display($template);
    

    
 

?>